import React from 'react';

import './Button.scss';

const Button = (props) => {
  const { className, children } = props;

  return (
    <button className={className} {...props}>{children}</button>
  )
};

Button.defaultProps = {
  className: 'btn btn-primary',
};

export default Button;
