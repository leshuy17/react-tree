import React from 'react';
import FileIcon, { defaultStyles } from 'react-file-icon';
import ReactTooltip from 'react-tooltip'

import './ExplorerItem.scss';

const getFileType = filename => filename.split('.').pop();
const getFileName = filename => filename.split('.').slice(0, -1).join('.');

const styles = {
  ...defaultStyles,
  'html': {
    labelColor: 'red',
    glyphColor: 'red',
  },
  'png': {
    labelColor: 'blue',
    glyphColor: 'blue',
  },
  'css': {
    color: '#2C5898',
    glyphColor: 'rgba(255,255,255,0.4)',
    labelColor: '#2C5898',
  },
  'lang': {
    labelColor: 'green',
    glyphColor: 'green',
  },
}

const ExplorerItem = ({
    item,
    projectPath,
    onEditItem,
    onDeleteItem
}) => (
    <div
        className="explorer-item"
    >
        <div className="explorer-item__inner">
            <div className="explorer-item__icon">
                <FileIcon extension={getFileType(item)} {...styles[getFileType(item)]} />
            </div>
            <div className="explorer-item__text">{getFileName(item)}</div>
            <div className="explorer-item__actions">
                <span
                    data-tip
                    data-for='explorer-edit'
                    className="explorer-item__edit explorer-item__btn"
                    onClick={onEditItem}
                />
                <span
                    data-tip
                    data-for='explorer-delete'
                    className="explorer-item__delete explorer-item__btn"
                    onClick={onDeleteItem}
                />
                <ReactTooltip id='explorer-edit' type="info">Edit</ReactTooltip>
                <ReactTooltip id='explorer-delete' type="error">Delete</ReactTooltip>
            </div>
        </div>
    </div>
);

export default ExplorerItem;
