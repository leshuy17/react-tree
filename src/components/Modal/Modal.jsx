import React from 'react';
import Modal from 'react-modal';

import './Modal.scss';

const customStyles = {
  content : {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    width: '100%',
    maxWidth: 600,
    padding: '40px 20px 20px'
  },
  overlay: {
    zIndex: 9,
    background: 'rgba(0,0,0, 0.4)',
  },
};

// Make sure to bind modal to your appElement (http://reactcommunity.org/react-modal/accessibility/)
Modal.setAppElement('body');

const BaseModal = ({ isOpen, children, close }) => (
  <Modal
    style={customStyles}
    closeTimeoutMS={300}
    isOpen={isOpen}
    onRequestClose={close}
  >
    <button
      className="close-modal"
      onClick={close}
    />
    {children}
  </Modal>
);

export default BaseModal;
