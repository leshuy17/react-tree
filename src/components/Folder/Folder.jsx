import React, { useState } from 'react';
import ReactTooltip from 'react-tooltip'

import './Folder.scss';

const Folder = ({
    children,
    title,
    onAddItem,
    onEditItem,
    onDeleteItem,
}) => {
     const [isOpen, toggleOpen] = useState(false);

    return (
        <div className="folder">
            <div className="folder__head">
                <div
                    className={`folder__toggle ${isOpen ? 'active' : ''}`}
                    onClick={() => toggleOpen(!isOpen)}
                />
                <div className="folder__top">
                    <div
                        className="folder__title"
                        onClick={() => toggleOpen(!isOpen)}
                    >
                        {title}
                    </div>
                  <div className="folder__actions">
                      <span
                          data-tip
                          data-for='folder-add'
                          className="folder__add folder__btn"
                          onClick={onAddItem}
                      />
                      <span
                          data-tip
                          data-for='folder-edit'
                          className="folder__edit folder__btn"
                          onClick={onEditItem}
                      />
                      <span
                          data-tip
                          data-for='folder-delete'
                          className="folder__delete folder__btn"
                          onClick={onDeleteItem}
                      />
                      <ReactTooltip id='folder-add' type="info">Add new item</ReactTooltip>
                      <ReactTooltip id='folder-edit' type="info">Edit</ReactTooltip>
                      <ReactTooltip id='folder-delete' type="error">Delete</ReactTooltip>
                  </div>
                </div>
            </div>
            {isOpen && (
                <div className="folder__body">
                    {children}
                </div>
            )}
        </div>
    )
};

export default Folder;
