import * as types from './actionTypes';

export const setUiState = (payload, meta) => ({
  type: types.SET_UI_STATE,
  payload,
  meta,
});
