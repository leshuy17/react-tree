// explorer reducer
export const RENAME_FIELD = 'RENAME_FIELD';
export const DELETE_FIELD = 'DELETE_FIELD';
export const ADD_FIELD = 'ADD_FIELD';

// ui reducer
export const SET_UI_STATE = 'SET_UI_STATE';
