import * as types from './actionTypes';

export const renameField = (payload) => {
  return { type: types.RENAME_FIELD, payload }
}

export const deleteField = (payload) => {
  return { type: types.DELETE_FIELD, payload }
}

export const addField = (payload) => {
  return { type: types.ADD_FIELD, payload }
}
