import * as actionTypes from '../actions/actionTypes';

import { changeTree, addField, deleteField } from './helpers';

const initState = {
    root: [
        {
           "CdmResource": [
              {
                 "dialog": [
                    "About.html",
                    "Main.html",
                    "Main8.html",
                    "Settings.html",
                    {
                       "image": [
                          "background.png",
                          "icon.png"
                       ]
                    }
                 ]
              },
              {
                 "diskspd": [
                    "diskspd32.exe",
                    "diskspd64.exe",
                    "LICENSE.txt"
                 ]
              },
              {
                 "language": [
                    "Bulgarian.lang",
                    "Catalan.lang",
                    "Croatian.lang",
                    "Czech.lang",
                    "Danish.lang",
                    "Dutch.lang",
                    "English.lang",
                    "Finnish.lang"
                 ]
              },
              {
                 "theme": [
                    {
                       "blue": [
                          "background.png",
                          "button.png",
                          "buttonHover.png",
                          "line.png",
                          "Main.css",
                          "transparent.png"
                       ],
                       "default": [
                          "background.png",
                          "button.png",
                          "buttonHover.png",
                          "line.png",
                          "Main.css",
                          "transparent.png"
                       ],
                       "flower": [
                          "background.png",
                          "button.png",
                          "buttonHover.png",
                          "line.png",
                          "Main.css",
                          "transparent.png"
                       ],
                       "Shizuku": [
                          "background.png",
                          "button.png",
                          "buttonHover.png",
                          "Main.css",
                          "meter.png",
                          "meterbg.png",
                          "transparent.png"
                       ],
                       "wine": [
                          "background.png",
                          "button.png",
                          "buttonHover.png",
                          "line.png",
                          "Main.css",
                          "transparent.png"
                       ]
                    }
                 ]
              }
           ],
        },
        {
           "License": [
              "COPYRIGHT.txt",
              "COPYRIGHT-ja.txt"
           ]
        }
    ]
}

const exporerReducer = (state = initState, action) => {
    if (action.type === actionTypes.RENAME_FIELD) {
      const { path, newValue } = action.payload;
      return {
          ...state,
          root: changeTree(state.root, path, newValue),
      }
    }

    if (action.type === actionTypes.ADD_FIELD) {
      const { path } = action.payload;

      return addField(state, path);
    }

    if (action.type === actionTypes.DELETE_FIELD) {
      return {
          ...state,
          root: deleteField(state.root, action.payload),
      };
    }

    return state;
}

export default exporerReducer;
