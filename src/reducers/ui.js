import * as type from '../actions/actionTypes';

const initialState = {
  newItemModal: {
    isOpen: false,
    initPath: '',
  },
  editItemModal: {
    isOpen: false,
    initPath: '',
  },
  deleteItemModal: {
    isOpen: false,
    initPath: '',
  },
};

const ui = (state = initialState, action) => {
  switch (action.type) {
    case type.SET_UI_STATE:
      return {
        ...state,
        [action.meta.key]: action.payload,
      };
    default:
      return state;
  }
};

export default ui;
