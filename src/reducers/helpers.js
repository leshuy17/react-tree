import * as R from 'ramda';

import { isArray, isObject, isString } from '../utils/index';

const renameKeys = R.curry((keysMap, obj) =>
  R.reduce((acc, key) => R.assoc(keysMap[key] || key, obj[key], acc), {}, R.keys(obj))
);

const getCorrectItem = str => str.includes('.') ? str : { [str]: [] };
const getCurrentPlace = str => str.split('/').filter(n => n)[0] || str;
const isFile = str => isString(getCorrectItem(str));

export const changeTree = (state, path, newValue) => {
  const pathToData = path.split('/').filter(n => n);
  const cur = getCurrentPlace(path);
  const next = path.replace(`/${cur}`, '');

  const target = pathToData[pathToData.length - 1];

  if (target === cur && isFile(cur)) {
    return state.map(item => {
      return item !== cur ? item : newValue;
    });
  }

  if (next) {
    return state.map(item => {
      if(R.has(cur)(item)) {
        return {
          ...item,
          [cur]: changeTree(item[cur], next, newValue)
        }
      }

      return item;
    })
  }

  return state.map(item => {
    if(R.has(cur)(item)) {
      return renameKeys({ [cur]: newValue })(item);
    }

    return item;
  });
}

const hasInIArry = (arr, prop) => arr.filter(item => {
    return (isString(item) && item === prop) || (isObject(item) && R.has(prop)(item));
})

const checkItemInData = (state, path, prevPath) => {
  const cur = getCurrentPlace(path);

  if(isObject(state)) return setObject(state, path, prevPath);

  if(isArray(state)) {
      if (isFile(cur)) return [...state, cur];

      return setArray(state, path);
  }

  return state;
}

const setObject = (data, path, prevPath) => {
    const prev = getCurrentPlace(prevPath);
    const cur = getCurrentPlace(path);
    const next = path.replace(`/${cur}`, '');

    if (next) {
        return {
            ...data,
            [prev]: checkItemInData([getCorrectItem(cur)], path)
        };
    } else {
        if(data[cur]) {
            return data;
        } else {
            return {
                ...data,
                [prev]: [getCorrectItem(cur)]
            }
        }
    }
}

const setArray = (data, path) => {
    const cur = getCurrentPlace(path);
    const next = path.replace(`/${cur}`, '');

    if (!cur) return data;

    if (next) {
        if (hasInIArry(data, cur).length > 0) {
            return data.map(obj => {
                const fondItem = hasInIArry(data, cur).find(o => R.identical(o, obj));
                return fondItem ? { ...obj, [cur]: checkItemInData(fondItem[cur], next, path) } : obj;
            });
        } else {
            return [ ...data, checkItemInData(getCorrectItem(cur), next, path) ];
        }
    } else {
        return hasInIArry(data, cur).length > 0 ? data : [ ...data, getCorrectItem(cur) ];
    }
}

export const addField = (state, fullPath) => {
  return {
      ...state,
      root: checkItemInData(state.root, fullPath)
  };
}

const remoteItem = (state, path) => {
    const cur = getCurrentPlace(path);
    let result = []

    state.forEach(obj => {
      if(R.has(cur)(obj)) {
        const pickItem = R.keys(obj).filter(item => item !== cur);
        const slicedItem = R.pickAll(pickItem, obj);

        if(!R.isEmpty(obj)) return result.push(slicedItem);
      }

      if(!R.has(cur)(obj)) result.push(obj);
    });

    return result;
}

export const deleteField = (state, path) => {
  const pathToData = path.split('/').filter(n => n);
  const target = pathToData[pathToData.length - 1];
  const cur = getCurrentPlace(path);
  const next = path.replace(`/${cur}`, '');

  if (isArray(state)) {
      if (next) {
          return state.map(obj => {
              if(R.has(cur)(obj)) {
                return R.keys(obj).reduce((acc, k) => {
                    if (k === cur) return { [k]: deleteField(obj[k], next) }

                    return { ...acc, [k]: obj[k] }
                }, {});
              }

              return obj;
          });
      } else {
          if (cur === target) {
            if(isFile(cur)) return R.without(cur, state);
          }
          return remoteItem(state, path)
      }
  }

  return state;
}
