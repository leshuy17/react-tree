import { combineReducers } from 'redux';
import explorer from './explorer';
import ui from './ui';

const rootReducer = combineReducers({
    path: explorer,
    ui,
});

export default rootReducer;
