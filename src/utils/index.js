export function isString (value) {
  return typeof value === 'string' || value instanceof String;
}

export function isArray (value) {
  return value && typeof value === 'object' && value.constructor === Array;
}

export function isObject (value) {
  return value && typeof value === 'object' && value.constructor === Object;
}

export function isArrayEmpty (value) {
  return isArray(value) && value.length === 0;
}
