import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { isArray, isObject, isString } from '../../utils/index';

import * as UiActions from '../../actions/ui';

import ExplorerItem from '../../components/ExplorerItem';
import Folder from '../../components/Folder';

import AddItemModal from './AddItemModal';
import EditItemModal from './EditItemModal';
import DeleteItemModal from './DeleteItemModal';

class Explorer extends Component {
    renderTreeNode = ({ children, projectPath }) => {

        const { setUiState } = this.props;

        if (isArray(children)) {
          return children.map(item => this.renderTreeNode({
              children: item,
              projectPath: projectPath,
          }));
        }

        if (isObject(children)) {
          return Object.keys(children).map((item, k) => {
            const path = `${projectPath}/${item}`;

            return (
                <Folder
                  key={item + k}
                  title={item}
                  onAddItem={() => setUiState({ isOpen: true, initPath: path }, { key: 'newItemModal' })}
                  onEditItem={() => setUiState({ isOpen: true, initPath: path }, { key: 'editItemModal' })}
                  onDeleteItem={() => setUiState({ isOpen: true, initPath: path }, { key: 'deleteItemModal' })}
                >
                    {this.renderTreeNode({
                        children: children[item],
                        projectPath: path
                    })}
                </Folder>
            )
          });
        }

        if (isString(children)) {
            const path = `${projectPath}/${children}`;

            return (
                <ExplorerItem
                    item={children}
                    projectPath={path}
                    onEditItem={() => setUiState({ isOpen: true, initPath: path }, { key: 'editItemModal' })}
                    onDeleteItem={() => setUiState({ isOpen: true, initPath: path }, { key: 'deleteItemModal' })}
                />
            )
        }
    }

    render() {
        const { path } = this.props;

        return (
            <>
                {this.renderTreeNode({
                    children: path,
                    projectPath: ''
                })}

                <AddItemModal />
                <EditItemModal />
                <DeleteItemModal />
            </>
        )
    }
}

const mapStateToProps = ({ path }) => ({
    path: path.root
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    ...UiActions,
  }, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Explorer);
