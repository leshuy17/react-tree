import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as UiActions from '../../actions/ui';
import * as ExplorerActions from '../../actions/explorer';

import Modal from '../../components/Modal';
import Button from '../../components/Button';

const EditItemModal = (props) => {
  const { initPath, isOpen, setUiState, deleteField } = props;

  const close = () => {
    setUiState({ isOpen: false, initPath: ''}, { key: 'deleteItemModal' });
  }

  return (
    <Modal
      isOpen={isOpen}
      close={close}
    >
      <div className="text-center fz21 alert mb10">All data will be deleted permanently.</div>

      <div className="text-center">
        <Button
          onClick={() => {
            deleteField(initPath);
            close();
          }}
        >
          Delete
        </Button>
      </div>
    </Modal>
  )
}

const mapStateToProps = ({ ui }) => ({
    isOpen: ui.deleteItemModal.isOpen,
    initPath: ui.deleteItemModal.initPath,
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    ...ExplorerActions,
    ...UiActions,
  }, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(EditItemModal);
