import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as UiActions from '../../actions/ui';
import * as ExplorerActions from '../../actions/explorer';

import Modal from '../../components/Modal';
import Button from '../../components/Button';

const EditItemModal = (props) => {
  const { initPath, isOpen, setUiState, addField } = props;
  const [path, setPath] = useState('');

  useEffect(() => {
    setPath(initPath + '/')
  }, [initPath]);

  const close = () => {
    setUiState({ isOpen: false, initPath: ''}, { key: 'newItemModal' });
  }

  return (
    <Modal
      isOpen={isOpen}
      close={close}
    >
      <input
        value={path}
        type="text"
        className="input-control input-control_big mb10"
        onChange={e => e.target.value.length === 0 ? setPath('/') : setPath(e.target.value)}
      />

      <div className="text-center">
        <Button
          onClick={() => {
            addField({ path });
            close();
          }}
        >
          Add
        </Button>
      </div>
    </Modal>
  )
}

const mapStateToProps = ({ ui }) => ({
    isOpen: ui.newItemModal.isOpen,
    initPath: ui.newItemModal.initPath,
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    ...ExplorerActions,
    ...UiActions,
  }, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(EditItemModal);
