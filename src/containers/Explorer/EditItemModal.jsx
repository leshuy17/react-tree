import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as UiActions from '../../actions/ui';
import * as ExplorerActions from '../../actions/explorer';

import Modal from '../../components/Modal';
import Button from '../../components/Button';

const EditItemModal = (props) => {
  const { initPath, isOpen, setUiState, renameField } = props;
  const [path, setPath] = useState('');

  useEffect(() => {
    setPath(initPath.split('/').pop())
  }, [initPath]);

  const close = () => {
    setUiState({ isOpen: false, initPath: ''}, { key: 'editItemModal' });
  }

  return (
    <Modal
      isOpen={isOpen}
      close={close}
    >
      <input
        value={path}
        type="text"
        className="input-control input-control_big mb10"
        onChange={e => e.target.value.length === 0 ? setPath('/') : setPath(e.target.value)}
      />

      <div className="text-center">
        <Button
          onClick={() => {
            renameField({ path: initPath, newValue: path });
            close();
          }}
        >
          Edit
        </Button>
      </div>
    </Modal>
  )
}

const mapStateToProps = ({ ui }) => ({
    isOpen: ui.editItemModal.isOpen,
    initPath: ui.editItemModal.initPath,
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    ...ExplorerActions,
    ...UiActions,
  }, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(EditItemModal);
