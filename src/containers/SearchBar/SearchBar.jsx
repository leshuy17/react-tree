import React, { useState } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import './SearchBar.scss'

import * as ExplorerActions from '../../actions/explorer';

import Button from '../../components/Button';

const SearchBar = (props) => {
  const { addField } = props;
  const [path, setPath] = useState('/');

  return (
    <div className="search-bar">
      <input
        value={path}
        type="text"
        className="input-control input-control_big mb10"
        onChange={e => e.target.value.length === 0 ? setPath('/') : setPath(e.target.value)}
      />

      <div className="text-center">
        <Button
          disabled={!path.length}
          onClick={() => {
              addField({ path });
              setPath('/');
          }}
        >
          Add
        </Button>
      </div>
    </div>
  )
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    ...ExplorerActions,
  }, dispatch);
}

export default connect(
    null,
    mapDispatchToProps,
)(SearchBar);
