import React, { Component } from 'react';
import { Provider } from 'react-redux';

import configureStore from './store';
import './App.scss';

import Explorer from './containers/Explorer';
import SearchBar from './containers/SearchBar';

const store = configureStore();

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <div className="explorer">
                    <SearchBar />
                    <Explorer />
                </div>
            </Provider>
        );
    }
}

export default App;
